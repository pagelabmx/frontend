// in src/posts.js
import React from 'react';
import {
    required,
    choices,
    minLength,
    maxLength,
    SelectInput,
    List,
    Edit,
    Create,
    Datagrid,
    DateField,
    TextField,
    EditButton,
    DeleteButton,
    DisabledInput,
    SimpleForm,
    TextInput,
    Filter
} from 'react-admin';

const ContactFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Buscar..." source="search" alwaysOn/>
    </Filter>
);


export const ContactList = (props) => (
    <List {...props} filters={<ContactFilter/>} title="Lista de Contactos">
        <Datagrid>
            <TextField label="ID" source="id" />
            <TextField label="UUID" source="custom_uuid" />
            <TextField label="NOMBRE" source="custom_fullname" />
            <TextField label="FECHA DE REGISTRO" source="created_at" />
            <EditButton />
            <DeleteButton/>
        </Datagrid>
    </List>
);

const ContactTitle = ({ record }) => {
    return <span>Contacto {record ? `${record.nombre}` : ''}</span>;
};

const validateSex = choices(['H', 'M'], 'Must be Male or Female');
const validateFirstName = [required(), minLength(2), maxLength(15)];
//const redirect = (basePath, id, data) => `/contact/`;

export const ContactEdit = (props) => (
    <Edit title={<ContactTitle />} {...props}>
        <SimpleForm redirect={'/contact'} submitOnEnter={true}>
            <DisabledInput source="id" />
            <DisabledInput source="uuid" />
            <TextInput label="Nombre(s)" source="nombre" validate={validateFirstName}/>
            <TextInput source="apellido_paterno" />
            <TextInput source="apellido_materno" />
            <SelectInput label="Sexo" source="sexo" choices={[
                { id: 'H', name: 'Hombre' },
                { id: 'M', name: 'Mujer' },
            ]} validate={validateSex}/>
            <DateField source="created_at" />
        </SimpleForm>
    </Edit>
);

export const ContactCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="nombre" />
            <TextInput source="apellido_paterno" />
            <TextInput source="apellido_materno" />
            <SelectInput label="Sexo" source="sexo" choices={[
                { id: 'H', name: 'Hombre' },
                { id: 'M', name: 'Mujer' },
            ]} validate={validateSex}/>
        </SimpleForm>
    </Create>
);