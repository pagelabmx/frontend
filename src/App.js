// in src/App.js
import React from 'react';
import { Admin, Resource} from 'react-admin';
import {ContactEdit, ContactCreate,  ContactList} from "./contacts";
import authProvider from './authProvider';
import dataProvider from './dataProvider';

const App = () => (
    <Admin title="Agenda" authProvider={authProvider} dataProvider={dataProvider}>
        <Resource title="Contactos" name="contact" create={ContactCreate} edit={ContactEdit} list={ContactList} />
    </Admin>
);

export default App;
