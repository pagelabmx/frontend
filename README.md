Este proyecto se inició con [Create React App](https://github.com/facebookincubator/create-react-app).
---

## Setup 

  * ### 1. Requerimientos
    * nodejs@8.10.0  
    * npm@6.1.0 (`$ npm i -g npm`)
    * yarn@1.7.0 (opcional) (`$ npm i -g yarn`)

  * ### 2. Instalación
    * `$ git clone https://perezatanaciod@bitbucket.org/pagelabmx/frontend.git`
    * `$ cd frontend && npm install` or `$ cd app && yarn install`


## Run
    El web server se debe estar ejectando en local en la siguiente dirección:puerto
    http://127.0.0.1:8000
    
  * ### dev
    * `$ yarn start`
    * Diríjase a http://127.0.0.1:3000/
    
  
  * Usuario y Contraseña Fake
    u: Daniel
    p: 123456


## Meta

Daniel Pérez Atanacio - [daniel.pagelab.io/](http://daniel.pagelab.io/) - contact@daniel.pagelab.io
